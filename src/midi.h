/** @file
 *  @brief MIDI Service
 */

#include <stdbool.h>
#include <zephyr/types.h>

#ifdef __cplusplus
extern "C" {
#endif

	/**@brief UUID of the MIDI Service. **/
#define BT_UUID_MIDI_VAL\
	BT_UUID_128_ENCODE(0x03b80e5a, 0xede8, 0x4b33, 0xa751, 0x6ce34ec4c700)

	/**@brief UUID of the Data IO Characteristic. **/
#define BT_UUID_MIDI_IO_VAL\
	BT_UUID_128_ENCODE(0x7772e5db, 0x3868, 0x4112, 0xa1a9, 0xf2669d106bf3)
#define BT_UUID_MIDI_SERVICE BT_UUID_DECLARE_128(BT_UUID_MIDI_VAL)
#define BT_UUID_MIDI_SERVICE_SHORT BT_UUID_DECLARE_16(0x0E5A)
#define BT_UUID_MIDI_IO BT_UUID_DECLARE_128(BT_UUID_MIDI_IO_VAL)

	struct flex_sensor
	{
		/**adc channel */
		uint8_t adcchan;
		/**channel*/
		uint8_t chanon;
		/**channel*/
		uint8_t chanoff;
		/**note */
		uint8_t note;
		/**low ADC reading */
		uint16_t low;
		/**high ADC reading */
		uint16_t high;
		/**current state 0 or 1 with 0 being NOTEOFF and 1 being NOTEON */
		uint8_t state;
		/**MIDI data */
	};


/** @brief Send data.
 *
 * @details This function sends data to a connected peer.
 *
 * @param[in] conn Pointer to connection Object.
 * @param[in] data Pointer to a data buffer.
 * @param[in] len  Length of the data in the buffer.
 *
 * @retval 0 If the data is sent.
 *           Otherwise, a negative value is returned.
 */
int midi_note(struct bt_conn *conn, uint8_t chan, uint8_t note, uint8_t state);

#ifdef __cplusplus
}
#endif
