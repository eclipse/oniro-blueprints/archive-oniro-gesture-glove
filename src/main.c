/*
 * Copyright (c) 2016 Intel Corporation.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/types.h>
#include <stddef.h>
#include <string.h>
#include <errno.h>
#include <zephyr.h>
#include <device.h>
#include <stdio.h>
#include <math.h>
#include <drivers/uart.h>
#include <drivers/gpio.h>
#include <drivers/sensor.h>
#include <sys/printk.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/conn.h>
#include <bluetooth/uuid.h>
#include <bluetooth/gatt.h>

#include "analogin.h"
#include "debugconsole.h"
#include "midi.h"

static struct bt_conn *current_conn;

#define LSM6DS0 DT_INST(0, st_lsm6ds0)

#if DT_NODE_HAS_STATUS(LSM6DS0, okay)
#define LSM6DS0_LABEL DT_LABEL(LSM6DS0)
#else
#error Your devicetree has no enabled nodes with compatible "st,lsm6ds0"
#define LSM6DS0_LABEL "<none>"
#endif

#define DEVICE_NAME CONFIG_BT_DEVICE_NAME
#define DEVICE_NAME_LEN (sizeof(DEVICE_NAME) - 1)

static struct flex_sensor indexf = {
.adcchan = 5, 
.chanon = 0x97, 
.chanoff = 0x87, 
.note = 0x30, 
.low = 85, 
.high = 108, 
.state = 0x00
};

static struct flex_sensor middlef = {
.adcchan = 6, 
.chanon = 0x97, 
.chanoff = 0x87, 
.note = 0x31, 
.low = 80, 
.high = 108, 
.state = 0x00
};

static struct flex_sensor ringf = {
.adcchan = 3, 
.chanon = 0x98, 
.chanoff = 0x88, 
.note = 0x30, 
.low = 80, 
.high = 105, 
.state = 0x00
};

static struct flex_sensor pinkyf = {
.adcchan = 2, 
.chanon = 0x98, 
.chanoff = 0x88, 
.note = 0x31, 
.low = 80, 
.high = 105, 
.state = 0x00
};


static const struct bt_data ad[] = { BT_DATA_BYTES(BT_DATA_FLAGS, 
	(BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR)),
	BT_DATA(BT_DATA_NAME_COMPLETE, DEVICE_NAME, DEVICE_NAME_LEN),
};

static const struct bt_data sd[] = { BT_DATA_BYTES(BT_DATA_UUID128_ALL,
	BT_UUID_MIDI_VAL),
};

static void connected(struct bt_conn *conn, uint8_t err)
{
	char addr[BT_ADDR_LE_STR_LEN];

	if (err)
	{
		printk("Connection failed (err 0x%02x)\n", err);
	};
	bt_addr_le_to_str(bt_conn_get_dst(conn), addr, sizeof(addr));
	printk("Connected to %s", log_strdup(addr));

	current_conn = bt_conn_ref(conn);
}

static void disconnected(struct bt_conn *conn, uint8_t reason)
{
	char addr[BT_ADDR_LE_STR_LEN];
	bt_addr_le_to_str(bt_conn_get_dst(conn), addr, sizeof(addr));
	printk("Disconnected: %s (reason %u)", log_strdup(addr), reason);
	if (current_conn)
	{
		bt_conn_unref(current_conn);
		current_conn = NULL;
	}
}

BT_CONN_CB_DEFINE(conn_callbacks) = { .connected = connected,
	.disconnected = disconnected,
};

static void auth_cancel(struct bt_conn *conn)
{
	char addr[BT_ADDR_LE_STR_LEN];

	bt_addr_le_to_str(bt_conn_get_dst(conn), addr, sizeof(addr));

	printk("Pairing cancelled: %s\n", addr);
}

static struct bt_conn_auth_cb auth_cb_display = { .cancel = auth_cancel,
};

static int set_sampling_freq(const struct device *dev)
{
	int ret = 0;
	struct sensor_value odr_attr;

	/*set accel/gyro sampling frequency to 12.5 Hz */
	odr_attr.val1 = 12.5;
	odr_attr.val2 = 0;

	ret = sensor_attr_set(dev, SENSOR_CHAN_ACCEL_XYZ,
		SENSOR_ATTR_SAMPLING_FREQUENCY, &odr_attr);
	if (ret != 0)
	{
		printf("Cannot set sampling frequency for accelerometer.\n");
		return ret;
	}

	return 0;
}

static int normalize_flex(int16_t sv, int16_t max, int16_t min)
{
	uint8_t ret;
	uint8_t val = (sv - min) / (max - min);
	if (val > 1)
	{
		ret = 0x7F;
		return ret;
	}
	ret = 0x00;
	return ret;

}

static inline float out_ev(struct sensor_value *val)
{
	return (val->val1 + (float) val->val2 / 1000000);
}

static float readslider(const struct device *dev)
{
	struct sensor_value x;
	int ret;
	/*lsm6dso gyro */
	sensor_sample_fetch_chan(dev, SENSOR_CHAN_ACCEL_XYZ);
	sensor_channel_get(dev, SENSOR_CHAN_ACCEL_X, &x);
	ret = ((out_ev(&x) *10) +90);

	if (ret > 127) {
	return(127);
	}
	else if (ret < 0) {
	return(0);
	}
	
	return(ret);
}

void main(void)
{
	int err;
	int16_t indexv;
	int16_t middlev;
	int16_t ringv;
	int16_t pinkyv;
    int8_t slider;
    int8_t slider_acc = 0;
	err = bt_enable(NULL);
	if (err)
	{
		printk("Bluetooth init failed (err %d)\n", err);
		return;
	}

	err = bt_le_adv_start(BT_LE_ADV_CONN, ad, ARRAY_SIZE(ad), sd,
		ARRAY_SIZE(sd));
	if (err)
	{
		printk("Advertising failed to start (err %d)", err);
	}
	printk("Advertising successfully started");

	bt_conn_auth_cb_register(&auth_cb_display);

	int log = 0;
	log = (0 == debugconsole_init());
	if (log)
	{
		printk("Debug Console started\n");
	}

	const struct device *lsm = DEVICE_DT_GET(DT_NODELABEL(accel));

	if (lsm == NULL)
	{
		printf("No device \"%s\" found.\n", LSM6DS0_LABEL);
		return;
	}

	if (set_sampling_freq(lsm) != 0) {
		printk("Gyro Weirdness\n");
	}

	while (1)
	{
	
		indexv = normalize_flex(analog_read(indexf.adcchan), indexf.low,
								indexf.high);
		if (indexv != indexf.state)
		{
			indexf.state = indexv;
			switch (indexv)
			{
				case 0:
					midi_note(current_conn, indexf.chanoff, indexf.note,
							  indexf.state);
				case 127:
					midi_note(current_conn, indexf.chanon, indexf.note,
							  indexf.state);
			};

			/*send midi packet */
		}

		middlev = normalize_flex(analog_read(middlef.adcchan), middlef.low,
								 middlef.high);
		if (middlev != middlef.state)
		{
			middlef.state = middlev;
			switch (middlev)
			{
				case 0:
					midi_note(current_conn, middlef.chanoff, middlef.note,
							  middlef.state);
				case 127:
					midi_note(current_conn, middlef.chanon, middlef.note,
							  middlef.state);
			};
			/*send midi packet */
		}

		ringv = normalize_flex(analog_read(ringf.adcchan), ringf.low,
							   ringf.high);
		if (ringv != ringf.state)
		{
			ringf.state = ringv;
			switch (ringv)
			{
				case 0:
					midi_note(current_conn, ringf.chanoff, ringf.note,
							  ringf.state);
				case 127:
					midi_note(current_conn, ringf.chanon, ringf.note,
							  ringf.state);
			};
			/*send midi packet */
		}

		pinkyv = normalize_flex(analog_read(pinkyf.adcchan), pinkyf.low,
								pinkyf.high);
		if (pinkyv != pinkyf.state)
		{
			pinkyf.state = pinkyv;
			switch (pinkyv)
			{
				case 0:
					midi_note(current_conn, pinkyf.chanoff, pinkyf.note,
							  pinkyf.state);
				case 127:
					midi_note(current_conn, pinkyf.chanon, pinkyf.note,
							  pinkyf.state);
			};
			/*send midi packet */
		}
		slider = (int)readslider(lsm);
		
		if (slider < 0) {
		slider = 0;
		} else if (slider > 127) {
		slider = 127;
		}

		if (slider > slider_acc+2 || slider < slider_acc-2) {
			midi_note(current_conn, 0xB0, 0x20, slider);
    		printk("Sending Crossfader Note %i, %i\n", slider, slider_acc);
		}
		slider_acc = slider;

/*

	
	while (1) {
    readslider(lsm);
        for (uint8_t i = 0; i < 7; i++) {
            int16_t sv = analog_read(i);
            printk("%i %d\n", i, sv);
        }
		printk("\0033\014");
        k_sleep(K_MSEC(1000));
        }
*/
	}

}

