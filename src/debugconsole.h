/* Copyright (C) 2021 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Bernhard Rosenkraenzer <bernhard.rosenkraenzer.ext@huawei.com>
 */
#pragma once

#ifndef NDEBUG
#include <usb/usb_device.h>
#include <drivers/uart.h>

BUILD_ASSERT(DT_NODE_HAS_COMPAT(DT_CHOSEN(zephyr_console), zephyr_cdc_acm_uart),
		"Console device is not ACM CDC UART device");

int debugconsole_init();
#else
static int debugconsole_init() { }
#endif
