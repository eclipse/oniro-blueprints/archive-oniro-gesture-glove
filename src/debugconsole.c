/* Copyright (C) 2021 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Bernhard Rosenkraenzer <bernhard.rosenkraenzer.ext@huawei.com>
 */

#include "debugconsole.h"

int debugconsole_init()
{
	const struct device *dev = DEVICE_DT_GET(DT_CHOSEN(zephyr_console));
	uint32_t dtr = 0;
	uint32_t retries = 0;

	if (usb_enable(NULL)) {
		return 1;
	}

	/* Poll if the DTR flag was set
	 * Time out after 2 seconds -- we might be
	 * connected to a power supply only
	 */
	while (!dtr && (retries++ < 20)) {
		uart_line_ctrl_get(dev, UART_LINE_CTRL_DTR, &dtr);
		/* Give CPU resources to low priority threads. */
		k_sleep(K_MSEC(100));
	}

	return !dtr;
}
